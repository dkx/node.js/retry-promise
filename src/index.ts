import {waitPromise} from '@dkx/wait-promise';


export declare type RetryOperationCallback<T> = (times: number) => Promise<T>;


export declare interface RetryPromiseOptions
{
	retries?: number,
	delay?: number,
	exponential?: boolean,
	shouldRetry?: (e, times: number) => boolean,
}


const retryDefaults: RetryPromiseOptions = {
	retries: 10,
	delay: 100,
	exponential: false,
	shouldRetry: () => true,
};


export function retryPromise<T>(operation: RetryOperationCallback<T>, options: RetryPromiseOptions = {}): Promise<T>
{
	options = {...retryDefaults, ...options};
	return doRetryPromise(operation, options, options.delay, 1);
}


async function doRetryPromise<T>(operation: RetryOperationCallback<T>, options: RetryPromiseOptions, delay: number, times: number): Promise<T>
{
	try {
		return await operation(times);
	} catch (e) {
		if (times === options.retries || !options.shouldRetry(e, times)) {
			throw e;
		}

		if (options.exponential) {
			delay = delay * 2;
		}

		await waitPromise(delay);
		return doRetryPromise(operation, options, delay, times + 1);
	}
}
