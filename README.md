# DKX/RetryPromise

Retry promise

## Installation

```bash
$ npm install --save @dkx/retry-promise
```

or with yarn

```bash
$ yarn add @dkx/retry-promise
```

## Usage

```typescript
import {retryPromise} from '@dkx/retry-promise';

(async () => {
	await retryPromise(async () => {
		await doSomething();
	}, {
		retries: 10,
		delay: 100,
		exponential: false,
		shouldRetry: (times) => {
			return findOutIfOperationShouldRetry(times); // return boolean
		},
	});
})();
```

All options are optional.
