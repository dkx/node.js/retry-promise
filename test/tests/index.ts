import {expect, use as chaiUse} from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import {performance} from 'perf_hooks';
import {retryPromise} from '../../lib';


chaiUse(chaiAsPromised);


describe('retryPromise()', () => {

	it('should run successfully the first time', async () => {
		let times = 0;

		await retryPromise(async (t) => {
			times++;
			expect(t).to.be.equal(times);
		});

		expect(times).to.be.equal(1);
	});

	it('should retry on error', async () => {
		const started = performance.now();
		let times = 0;

		await retryPromise(async (t) => {
			times++;

			expect(t).to.be.equal(times);

			if (times === 1) {
				throw new Error('Fail');
			}
		});

		const ended = performance.now();
		const elapsed = ended - started;

		expect(times).to.be.equal(2);
		expect(elapsed).to.be.greaterThan(99);
		expect(elapsed).to.be.lessThan(200);
	});

	it('should fail', async () => {
		let times = 0;

		await expect(retryPromise(async (t) => {
			times++;
			expect(t).to.be.equal(times);
			throw new Error('Fail');
		})).to.be.rejectedWith(Error, 'Fail');

		expect(times).to.be.equal(10);
	});

	it('should increase delay exponentially', async () => {
		const started = performance.now();

		await expect(retryPromise(async () => {
			throw new Error('Fail');
		}, {
			exponential: true,
			delay: 10,
			retries: 3,
		})).to.be.rejectedWith(Error, 'Fail');

		const ended = performance.now();
		const elapsed = ended - started;

		expect(elapsed).to.be.greaterThan(59);
		expect(elapsed).to.be.lessThan(70);
	});

	it('should retry based on callback', async () => {
		let times = 0;

		await retryPromise(async (t) => {
			times++;
			expect(t).to.be.equal(times);
		}, {
			shouldRetry: (t) => {
				expect(t).to.be.equal(times);
				return false;
			},
		});

		expect(times).to.be.equal(1);
	});

});
